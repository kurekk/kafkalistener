import datetime
from threading import Thread

import time

import logging

from kafkatest.estimator.anomallies.anomalies_estimator import AnomaliesEstimator
from kafkatest.estimator.anomallies.anomaly_judge import AnomalyJudge
from kafkatest.estimator.aysnc.model.med_times_msg import MedTimesMsg


class PredictAndRaiseAlertThread(Thread):
    def __init__(self, estimator: AnomaliesEstimator, anomaly_judge: AnomalyJudge, msg_bytes, span_time_secs=60,
                 ignore_older_than=2):
        super().__init__()
        self._logger = logging.getLogger(__name__)
        self.__estimator = estimator
        self.__anomaly_judge = anomaly_judge
        self.__msg_bytes = msg_bytes
        self.__span_time_secs = span_time_secs
        self.__ignore_older_than = ignore_older_than

    def run(self):
        deserialized_msg = self.__deserialize_msg(self.__msg_bytes)
        if self.__should_ignore(deserialized_msg):
            msg_datetime = self.__timestamp_to_datetime_str(deserialized_msg)
            self._logger.warning("Message with time %s expried" % msg_datetime)
            return
        self.__estimator.predict_proba(deserialized_msg.med_times)

    def __deserialize_msg(self, msg) -> MedTimesMsg:
        pass

    def __should_ignore(self, msg: MedTimesMsg):
        low_time_margin = self.__calculate_valid_low_time_margin()

        return int(msg.timestamp / self.__span_time_secs) < low_time_margin

    def __calculate_valid_low_time_margin(self):
        return int((time.time() - self.__span_time_secs * self.__ignore_older_than) / self.__span_time_secs)

    def __timestamp_to_datetime_str(self, msg: MedTimesMsg):
        return datetime.fromtimestamp(msg.timestamp).strftime("%Y-%m-%d %H:%M:%S,%f")
