from unittest import TestCase

import time

from datetime import datetime


class PredictAndRaiseAlertThread(TestCase):
    def test_timestamp(self):
        current_timestamp = int(time.time() / 60)
        print(self.__timestamp_to_datetime_str(current_timestamp))
        current_timestamp = int((time.time() - 120) / 60)
        print(self.__timestamp_to_datetime_str(current_timestamp))

    def __timestamp_to_datetime_str(self, timestamp: float, span_secs=60) -> str:
        return datetime.fromtimestamp(timestamp * span_secs).strftime("%Y-%m-%d %H:%M:%S,%f")
