class MedTimesMsg(object):
    def __init__(self, msg: dict):
        self.msg = msg

    @property
    def timestamp(self):
        return self.msg.get("timestamp", None)

    @property
    def server_name(self):
        return self.msg.get("server_name", None)

    @property
    def med_times(self):
        return self.msg.get("med_times", None)
