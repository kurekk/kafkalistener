from abc import ABCMeta, abstractmethod

import numpy as np


class AnomaliesEstimator(metaclass=ABCMeta):
    @abstractmethod
    def predict(self, x: np.ndarray):
        pass

    @abstractmethod
    def predict_proba(self, x: np.ndarray) -> (np.ndarray, np.ndarray, np.ndarray):
        pass
