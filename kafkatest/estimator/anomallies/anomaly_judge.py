from abc import ABCMeta, abstractmethod


class AnomalyJudge(metaclass=ABCMeta):
    @abstractmethod
    def is_anomaly(self):
        pass
