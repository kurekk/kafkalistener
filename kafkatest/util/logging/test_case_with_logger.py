import logging.config, yaml

from unittest import TestCase


class TestCaseWithLogger(TestCase):
    def __init__(self, logging_file, *args, **kwargs):
        super(TestCaseWithLogger, self).__init__(*args, **kwargs)
        with open(logging_file) as logging_config_file:
            config = yaml.safe_load(logging_config_file.read())
        logging.config.dictConfig(config)
