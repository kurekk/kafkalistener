from time import sleep
from unittest import TestCase

from datetime import datetime
from pykafka.client import KafkaClient
from pykafka.common import OffsetType


class TestKafka(TestCase):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.__client = KafkaClient()
        self.__test_topic = self.__client.topics[str.encode("test")]

    def setUp(self):
        self.assertIsNotNone(self.__test_topic)

    def test_kafka_producer(self):
        self.assertIsNotNone(self.__test_topic)
        producer = self.__test_topic.get_producer()
        producer.produce(self.__get_message("Hello World!"))

    def test_consume(self):
        msg_to_produce_cnt = 3
        producer = self.__test_topic.get_producer()
        for i in range(msg_to_produce_cnt):
            producer.produce(self.__get_message("Message number %d" % i))
            print("Produced messgae number %d." % i)

        consumer = self.__test_topic.get_simple_consumer(str.encode("test_group"),
                                                         auto_offset_reset=OffsetType.LATEST,
                                                         reset_offset_on_start=True,
                                                         consumer_timeout_ms=1000)
        self.assertIsNotNone(consumer)
        consumed_msgs_cnt = 0
        retry_num = 0
        while (consumed_msgs_cnt < msg_to_produce_cnt) and (retry_num < 60):
            m = consumer.consume()
            if m:
                print("Got message: '%s'" % m.value.decode("utf-8"))
                consumed_msgs_cnt += 1
            else:
                retry_num += 1

    def __get_message(self, text=""):
        return str.encode("[%s] %s" % (datetime.now().strftime("%Y-%m-%d %H:%M:%S,%f"), text))
