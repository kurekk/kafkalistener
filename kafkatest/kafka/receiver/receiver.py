from threading import Event

from pykafka import SimpleConsumer
from pykafka.client import KafkaClient
from pykafka.common import OffsetType
from typing import Dict


class KafkaReceiver:
    def __init__(self, kafka_client: KafkaClient, consumer_timeout_ms=1000, auto_offset_reset: OffsetType = None):
        self.__kafka_client = kafka_client
        self.__consumer_timeout_ms = consumer_timeout_ms
        self.__auto_offset_reset = auto_offset_reset or OffsetType.EARLIEST
        self.__consumers = {}  # type: Dict[SimpleConsumer]

    def receive_next(self, topic_name: str, block=False, unblock_event: Event = None):
        consumer = self.__get_consumer(topic_name)

        return consumer.consume(block, unblock_event)

    def receive_multiple(self, topic_name: str, block=False, unblock_event: Event = None, msg_limit=10):
        consumer = self.__get_consumer(topic_name)
        received_msgs = list()
        while msg_limit > 0:
            msg = consumer.consume(block, unblock_event)
            if not msg:
                break
            received_msgs.append(msg)
            msg_limit -= 1

        return received_msgs

    def __get_consumer(self, topic_name: str) -> SimpleConsumer:
        if topic_name not in self.__consumers:
            topic = self.__kafka_client.topics[str.encode(topic_name)]
            reset_offset_on_start = self.__auto_offset_reset == OffsetType.LATEST
            self.__consumers[topic_name] = topic.get_simple_consumer(consumer_timeout_ms=self.__consumer_timeout_ms,
                                                                     auto_offset_reset=self.__auto_offset_reset,
                                                                     reset_offset_on_start=reset_offset_on_start)

        return self.__consumers[topic_name]
