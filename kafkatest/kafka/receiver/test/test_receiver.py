import time
from unittest import TestCase

import itertools
from datetime import datetime
from pykafka import KafkaClient
from pykafka.common import OffsetType

from kafkatest.kafka.receiver.receiver import KafkaReceiver


class TestReceiver(TestCase):
    STR_TOPIC_NAME = "test"
    BYTES_TOPIC_NAME = str.encode(STR_TOPIC_NAME)

    @classmethod
    def setUpClass(cls):
        cls.__kafka_client = KafkaClient(broker_version="1.0.0")
        cls.__kafka_producer = cls.__kafka_client.topics[cls.BYTES_TOPIC_NAME].get_producer()

    def setUp(self):
        self.assertIsNotNone(self.__kafka_client)
        self.assertIsNotNone(self.__kafka_producer)

    def integration_test_receive(self):
        msg_to_sends = [self.__build_msg("Message %d" % i) for i in range(5)]
        for msg in msg_to_sends:
            self.__send_to_test_topic(msg)
        kafka_receiver = KafkaReceiver(self.__kafka_client)
        received_msgs = list()
        attemtps = 5
        while attemtps > 0:
            msg = kafka_receiver.receive_next(self.STR_TOPIC_NAME, True)
            if not msg:
                attemtps -= 1
            msg = self.__parse_msg(msg)
            if isinstance(msg, str):
                received_msgs.append(msg)
        self.assertEqual(msg_to_sends, received_msgs[-len(msg_to_sends):])

    def integration_test_receive_multiple(self):
        kafka_receiver = KafkaReceiver(self.__kafka_client, auto_offset_reset=OffsetType.LATEST)
        kafka_receiver.receive_next(self.STR_TOPIC_NAME)
        msg_to_sends = [self.__build_msg("Message %d" % i) for i in range(5)]
        for msg in msg_to_sends:
            self.__send_to_test_topic(msg)
            time.sleep(1)
        received_msgs = [m.value.decode("utf-8") for m in kafka_receiver.receive_multiple(self.STR_TOPIC_NAME, True)]
        self.assertEqual(len(received_msgs), len(msg_to_sends))
        self.assertEqual(msg_to_sends, received_msgs)

    def integration_test_receive_multiple_with_multi_parts(self):
        kafka_receiver = KafkaReceiver(self.__kafka_client, auto_offset_reset=OffsetType.LATEST)
        kafka_receiver.receive_next(self.STR_TOPIC_NAME)
        msg_to_sends = [self.__build_msg("Message %d" % i) for i in range(4)]
        for msg in msg_to_sends:
            self.__send_to_test_topic(msg)
            time.sleep(3)
        received_msgs = list(itertools.chain(
            *[kafka_receiver.receive_multiple(self.STR_TOPIC_NAME, True, msg_limit=2) for _ in range(2)]))
        print(received_msgs[0].timestamp)
        received_msgs = [m.value.decode("utf-8") for m in received_msgs]
        self.assertEqual(received_msgs, msg_to_sends)

    def __send_to_test_topic(self, msg: str):
        self.__kafka_producer.produce(str.encode(msg), timestamp=datetime.now())

    def __build_msg(self, msg: str):
        return "[%s] %s" % (datetime.now().strftime("%Y-%m-%d %H:%M:%S%f"), msg)

    def __parse_msg(self, msg_bytes):
        try:
            return msg_bytes.value.decode("utf-8")
        except:
            return msg_bytes
