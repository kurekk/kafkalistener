import time
from unittest import mock
from unittest.mock import MagicMock

from kafkatest.kafka.receiver.listener.kafka_listener import KafkaListener
from kafkatest.util.logging.test_case_with_logger import TestCaseWithLogger


class TestKafkaListener(TestCaseWithLogger):
    class DummyListener(KafkaListener):
        def _handle_msg(self, msg_bytes):
            pass

    def __init__(self, *args, **kwargs):
        super().__init__("logging.yaml", *args, **kwargs)

    def test_is_listener_handle_msgs(self):
        kafka_receiver = MagicMock()
        kafka_listener = self.DummyListener(kafka_receiver, "")
        with mock.patch.object(kafka_listener, "_handle_msg", wraps=kafka_listener._handle_msg) as mock_handle_msg:
            kafka_listener.start()
            time.sleep(3)
            self.__wait_for_stop(kafka_listener)
            self.assertFalse(kafka_listener.is_alive())
            self.assertTrue(mock_handle_msg.called)
            self.assertTrue(kafka_receiver.receive_multiple.called)

    def test_is_listener_handle_receive_exception(self):
        kafka_receiver = MagicMock()
        kafka_receiver.receive_multiple.side_effect = Exception
        kafka_listener = self.DummyListener(kafka_receiver, "")
        with mock.patch.object(kafka_listener, "_handle_receive_exception",
                               wraps=kafka_listener._handle_receive_exception) as mock_handle_ex:
            kafka_listener.start()
            time.sleep(3)
            self.__wait_for_stop(kafka_listener)
            self.assertFalse(kafka_listener.is_alive())
            self.assertTrue(kafka_receiver.receive_multiple.called)
            self.assertTrue(mock_handle_ex.called)

    def __wait_for_stop(self, kafka_listener: KafkaListener, attempts=10):
        while attempts > 0 and kafka_listener.is_alive():
            kafka_listener.stop_listener()
            time.sleep(1)
            attempts -= 1
