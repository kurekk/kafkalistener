import logging
import traceback
from abc import ABCMeta, abstractmethod
from threading import Thread, Lock
from typing import List

from kafkatest.kafka.receiver.receiver import KafkaReceiver


class KafkaListener(Thread, metaclass=ABCMeta):
    def __init__(self, kafka_receiver: KafkaReceiver, topic_name: str, is_consume_blocking=True, part_count=10):
        super().__init__()
        self._logger = logging.getLogger(__name__)
        self._is_consume_blocking = is_consume_blocking
        self.__stop = False
        self.__stop_lock = Lock()
        self.__kafka_receiver = kafka_receiver
        self.__topic_name = topic_name
        self.__part_count = part_count

    @abstractmethod
    def _handle_msg(self, msg_bytes: List[bytes]):
        pass

    def _handle_receive_exception(self, ex: Exception):
        self._logger.error("Reveive message exception:\n%s" % traceback.format_exc())

    def get_stop_lock(self) -> Lock:
        return self.__stop_lock

    def run(self):
        self._logger.info("Kafka listener started.")
        while not self.__should_stop():
            self.__receive_message()
        self._logger.info("Kafka listener stopped.")

    def stop_listener(self):
        self._logger.info("Stoping kafka listener...")
        self.__stop_lock.acquire(1)
        self.__stop = True
        self.__stop_lock.release()

    def __receive_message(self):
        try:
            self._logger.debug("Waiting for message...")
            msg = self.__kafka_receiver.receive_multiple(self.__topic_name, self._is_consume_blocking,
                                                         msg_limit=self.__part_count)
            self._logger.debug("Message received.")
            self._handle_msg(msg)
        except Exception as e:
            self._handle_receive_exception(e)

    def __should_stop(self) -> bool:
        self.__stop_lock.acquire()
        should_stop = bool(self.__stop)
        self.__stop_lock.release()

        return should_stop
